package com.test;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class MarvelCharactersService {

	public List<MarvelCharacter> getCharacters() throws JsonProcessingException, IOException {
		
		String url = WebConstants.GET_CHARACTERS + "?" + Utils.getMarvelAPIAuthenticationParameters();
		RestTemplate restTemplate = new RestTemplate();
		String result = restTemplate.getForObject(url, String.class);
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonRoot = mapper.readTree(result);
		JsonNode jsonResults = jsonRoot.get("data").get("results");
		
		List<MarvelCharacter> characters = mapper.readValue(jsonResults.toString(), new TypeReference<List<MarvelCharacter>>(){});
		
		return characters;
	}
}
