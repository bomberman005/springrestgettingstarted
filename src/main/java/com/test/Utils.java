package com.test;

import java.util.Date;

import org.springframework.util.DigestUtils;

public class Utils {

	public static String getMarvelAPIAuthenticationParameters() {
		
		String ts = String.valueOf(new Date().getTime());
		String apikey = WebConstants.PUBLIC_API_KEY;
		String hash = DigestUtils.md5DigestAsHex((ts 
						+ WebConstants.PRIVATE_API_KEY 
						+ WebConstants.PUBLIC_API_KEY).getBytes());
		
		return "ts=" + ts + "&apikey=" + apikey + "&hash=" + hash;
	}
}
