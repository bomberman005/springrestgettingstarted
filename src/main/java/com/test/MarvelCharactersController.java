package com.test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MarvelCharactersController {

	@Autowired
	private MarvelCharactersService marvelCharactersService;
	
	@RequestMapping("/characters")
	public ResponseEntity<List<MarvelCharacter>> getCharacters() {
		
		List<MarvelCharacter> characters = null;
		
		try {
			characters = marvelCharactersService.getCharacters();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<List<MarvelCharacter>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
			
		return new ResponseEntity<List<MarvelCharacter>>(characters, HttpStatus.OK);
	}
}
