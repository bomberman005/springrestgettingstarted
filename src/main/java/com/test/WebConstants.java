package com.test;

public interface WebConstants {

	String PRIVATE_API_KEY = "";
	String PUBLIC_API_KEY = "";
	
	String GET_CHARACTERS = "http://gateway.marvel.com:80/v1/public/characters";
}
